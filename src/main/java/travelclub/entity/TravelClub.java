package travelclub.entity;

import travelclub.util.DateUtil;

import java.util.UUID;

public class TravelClub {
  private static final int MINIMUM_NAME_LENGTH = 3;
  private static final int MINIMUM_INTRO_LENGTH = 10;
  private String id;
  private String clubName;
  private String intro;
  private String foundationDay;

  private TravelClub() {
    this.id = UUID.randomUUID().toString();
  }

  public TravelClub(String clubName, String intro) {
    this();
    setClubName(clubName);
    setIntro(intro);
    this.foundationDay = DateUtil.today();
  }

  public void setClubName(String clubName) {
    if (clubName.length() < MINIMUM_NAME_LENGTH) {
      throw new Error("클럽명은 " + MINIMUM_NAME_LENGTH + "자리 보다 작을 수 없습니다");
    }
    this.clubName = clubName;
  }

  public String getClubName() {
    return this.clubName;
  }

  public void setIntro(String intro) {

    if (intro.length() < MINIMUM_INTRO_LENGTH) {
      throw new Error("인트로는 " + MINIMUM_INTRO_LENGTH + "자리 보다 작을 수 없습니다");
    }
    this.intro = intro;
  }

  public String getIntro() {
    return this.intro;
  }

  public String getFoundationDay() {
    return foundationDay;
  }

  static public TravelClub getSample() {
    String clubName = "Sample Club";
    String intro = "This is a Sample Club";
    TravelClub tc = new TravelClub(clubName, intro);
    return tc;
    // 변수 인라인화
//    return new TravelClub("Sample Club", "This is Sample Club");
  }

  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("Travel Club => ");
    sb.append(" id: ").append(id);
    sb.append(", clubName: ").append(clubName);
    sb.append(", intro: ").append(intro);
    sb.append(", FoundationDay: ").append(foundationDay);

    return sb.toString();
  }
}
