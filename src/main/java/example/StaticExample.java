package example;

public class StaticExample {
  // 초기화가 가능한 위치
  private final String name = "꾸인수"; // (1)

  {
//    name = "꾸인수"; // (2)
  }

  public StaticExample() {
//    this.name = "꾸인수"; // (3)
  }

  public static void showName(final String name) {
//    name = "new name";
    // final parameter name may not be assigned
    System.out.println(name);
  }
}


