package example;

public class CalculatorOverloading {
  public static void main(String[] args) {
    Calculator calc = new Calculator();
    System.out.println(calc.sum(10, 20));
    System.out.println(calc.sum(10, 20, 30));
    System.out.println(calc.sum(10.1, 20.2));
  }
}
