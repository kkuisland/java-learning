package example;

// Overloading 학습
public class Calculator {
  // 합 => int 두개
  public int sum(int x, int y) {
    return x + y;
  }

  // 합 => int 세개
  public int sum(int x, int y, int z) {
    return x + y + z;
  }

  // 합 => double 두개
  public double sum(double x, double y) {
    return x + y;
  }
}
