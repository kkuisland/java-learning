package example;

public class Main {
  public static void main(String[] args) {
    System.out.println("Hello World");
    args = new String[3]; // 3개의 저장공간을 가진 배열 초기화
    args[0] = "안녕1"; // 1번째 배열
    args[1] = "안녕2"; // 2번째 배열
    args[2] = "안녕3"; // 3번째 배열

    System.out.println(args.length);
    System.out.println(args[0]);
    System.out.println(args[1]);
    System.out.println(args[2]);

    ThisConstructor tc = new ThisConstructor("아이디");
    ThisConstructor tcc = new ThisConstructor("아이디", "name");
    ThisConstructor tccc = new ThisConstructor("아이디", "이름", "개발부");
  }
}
