package example;

public class Account {
  private double balance;

  /* 생성자 */
  public Account(double balance) {
    this.balance = balance;
  }

  /* 입금 */
  public void deposit(double amount) {
    this.balance += amount;
  }

  /* 출금 */
  public double withdraw(double amount) {
    if (this.balance < amount) {
      System.out.println("잔액이 부족합니다");
    } else {
      this.balance -= amount;
    }
    return amount;
  }

  /* 잔액 */
  public double getBalance() {
    return this.balance;
  }

}
