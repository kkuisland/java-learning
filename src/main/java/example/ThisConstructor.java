package example;

public class ThisConstructor {
  private String id;
  private String name;
  private String department;

  public ThisConstructor(String id) {
    this.id = id;
    System.out.println("example.ThisConstructor(id) 호출: " + this.id);
  }

  public ThisConstructor(String id, String name) {
    this(id);
    this.name = name;
    System.out.println("example.ThisConstructor(id, name) 호출: " + this.name);
  }

  public ThisConstructor(String id, String name, String department) {
    this(id, name);
    this.department = department;
    System.out.println("example.ThisConstructor(id, name, department) 호출: " + this.department);
  }

}
