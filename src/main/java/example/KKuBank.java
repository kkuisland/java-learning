package example;

public class KKuBank {
  public static void main(String[] args) {
    // 인스턴스화 (객체생성: 인스턴스 메소드 사용)
    Account kkuAccount = new Account(10000);
    System.out.println("1. 10,000 을 입금하며 계좌를 개설합니다.. Total Balance: " + kkuAccount.getBalance());
    // 입금 => 전달인자 10000
    kkuAccount.deposit(10000);
    System.out.println("2. 10,000 을 입금합니다. Total Balance: " + kkuAccount.getBalance());
    // 출금 => 전달인자 15000
    kkuAccount.withdraw(15000);
    System.out.println("3. 15,000 을 출금 합니다. Total Balance: " + kkuAccount.getBalance());
  }
}
